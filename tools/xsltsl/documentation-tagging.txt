

= Using tagging =


== Using the fetch-news template ==

- the "tag" parameter defines what news will be displayed. If you are
  building up a country page, this is your country code. Leave blank if you
  wish to receive news items with any tags
    
    <xsl:with-param name="tag" select="''"/>
    
- the "nb-items" parameter determines how many news items will be displayed
    
    <xsl:with-param name="nb-items" select="''" />
    

== Using the fetch-newsletter template ==

Works exactly as the fetch-news template, but does not propose tagging yet.


== Using the fetch-events template ==

fetch-events works in a very similar manner to fetch-news, tag and nb-items
work the same way. Here are some addings:

- the "wanted-time" parameter defines what events you want to see, "past",
  "present" or "future"
    
    <xsl:with-param name="wanted-time" select="'future'" />
    
- the "header" parameter determines the text that will be displayed on top
  of your block of events (used e.g. on events/events.xsl)
  possible values, 'future', 'current' or 'past'
    
    <xsl:with-param name="header" select="'future'" />
    
- the "display-details" parameter says if 'yes' or 'no' you will have
  the details of each event displayed along with its date and title
    
    <xsl:with-param name="display-details" select="'yes'" />
    


